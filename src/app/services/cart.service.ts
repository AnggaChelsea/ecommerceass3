import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cart } from '../models/cart';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CartService {
   public toggleCartSubject = new BehaviorSubject(false);
   public cartListSubject = new BehaviorSubject([]);

//    data$:Observable<Model[]>

  constructor(private http:HttpClient) { }

   toggleCart = () =>{
        this.toggleCartSubject.next(!this.toggleCartSubject.getValue())
   }
    addCart = (cart:Cart) =>{
        let data= this.cartListSubject.getValue();
        let arrData = data.find(c=>c.product.name === cart.product.name);

        if(arrData) arrData.quantity += cart.quantity;
        else data.push(cart)
        this.cartListSubject.next(data)


    }
    tambahCart(cart){
      return this.http.post<any>(environment.urlAddress + 'cart', cart)
    }

    getCart(){
        //this.http.get(URL)
    }

    hpausCart(id_cart){
        //this.http.delete(url+id)
    }
    reloadCart = (cartList) =>{
        this.cartListSubject.next(cartList)
    }

    removeCart = index =>{
        let psoisi = this.cartListSubject.getValue();
        psoisi.splice(index,1);
        this.cartListSubject.next(psoisi)
    }


}
