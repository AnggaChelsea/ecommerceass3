import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { SigninComponent } from '../user/signin/signin.component';
import { User } from '../../models/user';
import { Observable } from 'rxjs'
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  username$:Observable<User[]>;
  /**
   * jumlah cart
   */
  public jumlah_cart : number;
  cart:any
  constructor(public authService:AuthService, private cartService:CartService) { }

  ngOnInit(): void {
    this.cartService.cartListSubject.subscribe(res=>{
      this.jumlah_cart = res.length
    })
  }

  toggleCartPopup = (event)=>{
    event.preventDefault()
    event.stopPropagation()
    this.cart=this.cartService.toggleCart()
    console.log("data",this.cart);


  }


}
